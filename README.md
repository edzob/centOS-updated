# CentOS updated
This repository is a simple centOS container.   
CentOS Base install updated via yum update.

**Table of Contents** 
- [CentOS updated](#centos-updated)
- [TL;DR](#tl;dr)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
    - [Select place for running docker daemon](#select-place-for-running-docker-daemon)
    - [Setup VM (gitlab-keys & docker & docker-compose)](#setup-vm-(gitlab-keys-&-docker-&-docker-compose))
    - [Docker-Compose extra reading material.](#docker-compose-extra-reading-material.)
  - [Run it](#run-it)
  - [Update it](#update-it)
- [Built With](#built-with)
  - [Devices](#devices)
  - [CodeEditor](#codeeditor)
  - [Programming Language(s)](#programming-language(s))
  - [Tools](#tools)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)
  - [Inspiration on Dockerfile](#inspiration-on-dockerfile)
  - [Inspiration on Docker & CentOS](#inspiration-on-docker & centos)
  - [Inspiration on Readme.md](#inspiration-on-readme.md)
  - [Inspiration on Markdown Syntax](#inspiration-on-markdown syntax)

## TL;DR
`git clone git@gitlab.com:edzob/centOS-updated.git`

`cd centOS-updated`

`docker-compose build`

`docker-compose run --rm centos-updated /bin/bash`

## Getting Started
This code will give you a docker container with the most recent CentOS and updated.

### Prerequisites
You need a machine with docker and it would help to also have docker-compose installed.

#### Select place for running docker daemon
For this project we used Google Cloud - Compute engine
(Micro VM, 600MB Ram, 10GB Disk, Debian, us-central1-f)

This is a free VM, that has external IP adres and will run for free. This VM can be created via the [Google Cloud Console][googleCloud01]
```
Your first 672 hours of f1-micro instance usage are free this month.
```
#### Setup VM (gitlab-keys & docker & docker-compose)
1. Spinup the VM
1. Loginto the VM via a console (ssh (root) access)
1. Create SSH keys on VM and add to Git Management Tool ([GitLab Instructions][gitlab01])
1. Install the [Docker Engine][dockerCE00] with the [Debian installation instructions][dockerCE01]
  1. [Post installation][dockerCE03] Steps (don't forget the reboot)
  1. Test docker via [Hello World][dockerCE02] and test script hidden in hello world
1. [Install][dockerCompose01] [docker compose][dockerCompose02]
  1. Test Docker Compose as part of installation step
`docker-compose --version`
  1. Clone project directory into vm
`git clone git@gitlab.com:edzob/centOS-updated.git`
  1. Test docker compose via [Google Cloud example][dockerCompose06]

[googleCloud01]: https://console.cloud.google.com/compute/
[gitlab01]: https://gitlab.com/help/ssh/README
[dockerCE00]: https://docs.docker.com/install/
[dockerCE01]: https://docs.docker.com/install/linux/docker-ce/debian/#install-using-the-repository
[dockerCE02]: https://docs.docker.com/get-started/#test-docker-version
[dockerCE03]: https://docs.docker.com/install/linux/linux-postinstall/
[dockerCompose01]: https://docs.docker.com/compose/install/
[dockerCompose02]: https://docs.docker.com/compose/install/#install-compose
[dockerCompose06]: https://cloud.google.com/community/tutorials/docker-compose-on-container-optimized-os

#### Docker-Compose extra reading material.
1. More [advanced example][dockerCompose03] of Docker Compose with multiple services devined in the YML file.
1. This [docker compose tutorial][dockerCompose05] by Nick Janetakis is using the repository: build-a-saas-app-with-flask
1. [Howto use docker-compose to Start, Stop, Remove Docker Containers][dockerCompose07] by TheGeekStuff
1. This page descripes the syntax of the [Docker Compose YML file][dockerCompose08]

[dockerCompose03]: https://docs.docker.com/compose/gettingstarted/
[dockerCompose05]: https://nickjanetakis.com/blog/dockerize-a-flask-celery-and-redis-application-with-docker-compose
[dockerCompose07]: https://www.thegeekstuff.com/2016/04/docker-compose-up-stop-rm/
[dockerCompose08]: https://docs.docker.com/compose/compose-file/#long-syntax-1

### Run it
1. Login on your vm
2. go to the project dir for example    
`cd Projects/centOS-updated`
3. build the container manually or via docker compose.    
`docker-compose build` or `docker build -t centos-updated .`
4. go into the container and play around.    
`docker-compose run --rm centos-updated /bin/bash` or `docker run -ti centos-updated bash`

### Update it
from the [centOS manpage](https://www.centos.org/docs/5/html/5.1/Deployment_Guide/s1-yum-useful-commands.html)
`yum check-update`

from the [centOS manpage](https://www.centos.org/docs/5/html/yum/sn-updating-your-system.html)
`su -c 'yum update'`

## Built With
### Devices
* Chromebook
* Google Cloud Computing

### CodeEditor
* Chromebook
    * [Cloud9](https://ide.c9.io/) Code editor
* Google cloud
    * vi

### Programming Language(s)
* Bash
* Dockerfile
* YML

### Tools
* [Docker](https://docs.docker.com)
* [Docker-compose](https://docs.docker.com/compose)

## Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Edzo Botjes** - *Initial work* - [Gitlab](https://gitlab.com/edzob), [Blogger](http://blog.edzob.com/), [Medium](https://medium.com/@edzob), [LinkedIN](https://www.linkedin.com/in/edzob/), [twitter](https://twitter.com/edzob) 

See also the list of [contributors](https://gitlab.com/edzob/centOS-updated/graphs/master) who participated in this project.

## License
```
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   
#    For more see the file 'LICENSE' for copying permission.
```

## Acknowledgments
### Inspiration on Dockerfile
* Dockefile code is 99% inspired by [Scott McCarty][scott01]

### Inspiration on Docker & CentOS
* "[Create your first Docker container : A Beginner’s guide][dockerRHEL02]" written by SHUSAIN 
  * This is quick install docker and run a centOS guide.
* "[Quick Containers with Fedora Dockerfiles][dockerRHEL03]" by Joe Brockmeier
  * This is quick install docker and run a fedora nginx guide. 
* "[Difference between CentOS, Fedora, and RHEL][dockerRHEL04]" written by Michael Boelen
  * A quick overview on the difference of these three closely connected distributions.
* "[GETTING STARTED WITH CONTAINERS][dockerRHEL09]" written by RedHat
  * A very extensive guide on containers and how to run them on Redhat. Serious business. 

[dockerRHEL09]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html-single/getting_started_with_containers/index#working_with_docker_registries
[dockerRHEL04]: https://linux-audit.com/difference-between-centos-fedora-rhel/
[dockerRHEL03]: https://fedoramagazine.org/quick-containers-with-fedora-dockerfiles/
[dockerRHEL02]: http://linuxtechlab.com/create-first-docker-container-beginners-guide/


### Inspiration on Readme.md
* Inspiration on Readme.md
  * Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
  * Github user jxson wrote: "[README.md template][readme02]"
  * Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

### Inspiration on Markdown Syntax
* Inspiration on Markdown Syntax
    * GitHub   
      * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
      * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
      * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
      * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
    * GitLab
      * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
      * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"

[scott01]: https://developers.redhat.com/blog/2014/05/15/practical-introduction-to-docker-containers/
[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/