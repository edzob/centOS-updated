#
# Version 1

# Pull from CentOS RPM Build Image
FROM centos

MAINTAINER Scott McCarty smccarty@redhat.com

# Update the image
RUN yum install deltarpm -y
RUN yum update -y

CMD /bin/sh

# Output
# ENTRYPOINT tail /var/log/yum.log
